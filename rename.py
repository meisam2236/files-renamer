import os
from os.path import isfile, join

mypath = os.getcwd()
onlyfiles = [f for f in os.listdir(mypath) if isfile(join(mypath, f))]
formated = '\The Big Bang Theory S03x'

for i in onlyfiles:
    # The.Big.Bang.Theory.S02E01.srt
    if(len(i.split())==1):
        parts = i.split('.')
        for s in parts:
            if s.startswith('S03E'):
                old_name = mypath + '\\' + i
                new_name = mypath + formated + s[4:] + '.' + parts[-1]
                os.rename(old_name, new_name)

    else:
        name = i.split('.')[0]
        parts = name.split()
        # 19-The Big Bang Theory.srt
        if parts[0].split('-')[0].isdigit():
            old_name = mypath + '\\' + i
            new_name = mypath + formated + parts[0].split('-')[0] + '.' + i.split('.')[1]
            os.rename(old_name, new_name)
        # The Big Bang Theory 10.srt
        for s in parts:
            if s.isdigit():
                old_name = mypath + '\\' + i
                new_name = mypath + formated + s + '.' + i.split('.')[1]
                os.rename(old_name, new_name)
            # The Big Bang Theory S02E01.srt
            elif s.startswith('S03E'):
                old_name = mypath + '\\' + i
                new_name = mypath + formated + s[4:] + '.' + i.split('.')[-1]
                os.rename(old_name, new_name)

print('Operation completed successfully!')